<?php
include_once '../Libreria/Conf/Connection.php';

class MasterModel extends Connection{

    public function insertar($sql){
        $respuesta=  pg_query($this->getConnect(), $sql);
        return $respuesta;
    }

    public function eliminar($sql){
        $respuesta=  pg_query($this->getConnect(), $sql);
        return $respuesta;
    }

    public function editar($sql){
        $respuesta=  pg_query($this->getConnect(), $sql);
        return $respuesta;
    }

    public function consultar($sql){
        $respuesta=  pg_query($this->getConnect(), $sql);
        return $respuesta;
    }
	
	public function consultarArray($sql){
		$respuesta=pg_query($this->getConnect(), $sql);
		$arreglo=array();
		if($respuesta){
			while($row= pg_fetch_array($respuesta)){
				$arreglo[]=$row;
			}
		}
		return $arreglo;
	}

    public function autoIncrement($table,$field){
      $sql="select max($field) from $table";
      $respuesta=$this->consultar($sql);
      $contador=pg_fetch_row($respuesta);
      return end($contador)+1;
    }

	public function autoDecrement($table,$field){
      $sql="select max($field) from $table";
      $respuesta=$this->consultar($sql);
      $contador=pg_fetch_row($respuesta);
      return end($contador)-1;
    }

    public function lastInsertId($table,$field){
      $sql="select max($field) from $table";
      $respuesta=$this->consultar($sql);
      $contador=pg_fetch_row($respuesta);
      return end($contador);
    }
}

?>
