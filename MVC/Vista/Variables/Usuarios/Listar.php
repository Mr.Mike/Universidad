 <ol class="breadcrumb">
	<li class="breadcrumb-item">
	  <a>Electivas</a>
	</li>
</ol>
 <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Listado</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Documento</th>
                  <th>Tipo de Documento</th>
				  <th>Rol</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Nombre</th>
                  <th>Documento</th>
                  <th>Tipo de Documento</th>
				  <th>Rol</th>
                </tr>
              </tfoot>
              <tbody>
				<?php
					for($i=0; $i<count($usuarios); $i++){
				?>
						<tr>
						  <td><?php echo $usuarios[$i][3]." ".$usuarios[$i][4];?></td>
						  <td><?php echo $usuarios[$i][6];?></td>
						  <td><?php echo $usuarios[$i][8];?></td>
						  <td><?php echo $usuarios[$i][11];?></td>
						</tr>
				<?php	
					}
				?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted"></div>
    </div>
    
    