 <ol class="breadcrumb">
	<li class="breadcrumb-item">
	  <a>Electivas</a>
	</li>
</ol>
 <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Listado</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Descripci&oacute;n</th>
				  <th>Cupos</th>
				  <th>Nº Estudiantes</th>
                  <th>Edici&oacute;n</th>
                  <th>Eliminaci&oacute;n</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Nombre</th>
                  <th>Descripci&oacute;n</th>
                  <th>Cupos</th>
                  <th>Nº Estudiantes</th>
                  <th>Edici&oacute;n</th>
                  <th>Eliminaci&oacute;n</th>
                </tr>
              </tfoot>
              <tbody>
				<?php
					for($i=0; $i<count($electivas); $i++){
				?>
						<tr>
						  <td><?php echo $electivas[$i][2];?></td>
						  <td><?php echo $electivas[$i][3];?></td>
						  <td><?php echo $electivas[$i][4];?></td>
						  <td><?php echo $electivas[$i][5];?></td>
						  <td><a href="<?php echo getUrl('Variables','Electivas','getEditar',array("id"=>$electivas[$i][0]))?>" > <button class='btn btn-primary' >Editar</button></a></td>
						  <td><a onClick="eliminar(<?php echo $electivas[$i][0];?>)"> <button class='btn btn-danger' >Eliminar</button></a></td></td>
						</tr>
				<?php	
					}
				?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted"></div>
    </div>
    
    