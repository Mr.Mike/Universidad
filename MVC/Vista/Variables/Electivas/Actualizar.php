<ol class="breadcrumb">
	<li class="breadcrumb-item">
	  <a>Actualizar</a>
	</li>
</ol>
<form method="post" action="<?php echo getUrl("Variables","Electivas","postEditar");?>">
	<div class="form-group">
		<div class="form-row">
			<div class="col-md-12">
			<?php
				for($i=0; $i<count($electivas); $i++){
			?>
				<input name="Id" type="hidden" value="<?php echo $electivas[$i][0]?>"/>
				
				<label for="exampleInputName">Nombre de la Electiva</label>
				<input name="Nombre" class="form-control" id="exampleInputName" type="text" value="<?php echo $electivas[$i][2]?>" required />
				
				<label for="selectTeacher">Instructor de la Electiva</label>
				<select name="Profesor" id="selectTeacher" class="form-control" required>
					<option value="<?php echo $electivas[$i][1]?>"><?php echo $electivas[$i][9]." ".$electivas[$i][10];?></option>
					<?php
					for($j=0; $j<count($profes); $j++){
						if($profes[$j][0]!=$electivas[$i][1]){
							echo "<option value=".$profes[$j][0].">".$profes[$j][3]." ".$profes[$j][4]."</option>";
						}
					}
					?>
				
				</select>
				
				<label for="exampleInputStudens">Cupos de la Electiva</label>
				<input name="Cupos" class="form-control" id="exampleInputStudens" type="number" value="<?php echo $electivas[$i][4]?>" required />
				
				<label for="exampleInputDescription">Descripcion de la Electiva</label>
				<textarea id="exampleInputDescription" rows="15" cols="100" name="Descripcion" class="form-control" minlength="20" required ><?php echo $electivas[$i][3]?></textarea>
			<?php
				}
			?>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<input class="btn btn-primary btn-block" type="submit" value="Actualizar" />
	</div>
</form>