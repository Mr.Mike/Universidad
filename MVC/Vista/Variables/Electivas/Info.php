<ol class="breadcrumb">
	<li class="breadcrumb-item">
	  <a>Informaci&oacute;n</a>
	</li>
</ol>
<form>
	<div class="form-group">
		<div class="form-row">
			<div class="col-md-12">
			<?php
				for($i=0; $i<count($electivas); $i++){
			?>
				<input name="Id" type="hidden" value="<?php echo $electivas[$i][0]?>"/>
				
				<label for="exampleInputName">Nombre de la Electiva</label>
				<input name="Nombre" class="form-control" id="exampleInputName" type="text" value="<?php echo $electivas[$i][2]?>" readonly />
				
				<label for="selectTeacher">Instructor de la Electiva</label>
				<input name="Nombre" class="form-control" id="exampleInputName" type="text" value="<?php echo $electivas[$i][9]." ".$electivas[$i][10];?>" readonly />
				
				<label for="exampleInputStudens">Cupos de la Electiva</label>
				<input name="Cupos" class="form-control" id="exampleInputStudens" type="number" value="<?php echo $electivas[$i][4]?>" readonly />
				
				<label for="exampleInputDescription">Descripcion de la Electiva</label>
				<textarea id="exampleInputDescription" rows="15" cols="100" name="Descripcion" class="form-control" minlength="20" readonly ><?php echo $electivas[$i][3]?></textarea>
				
				<label>Alunmos Inscritos</label>
				<ol>
					<?php
						for($i=0; $i<count($alumnos); $i++){
					?>
						<li><?php echo $alumnos[$i][3]." ".$alumnos[$i][4];?></li>
					<?php
						}
					?>
				</ol>
				<div class="modal-footer">
					<a onClick="inscripcion(<?php echo $electivas[$i][0];?> , <?php echo $_SESSION['codigo'];?>)" > <button class='btn btn-primary btn-block' >Participar</button></a>
				</div>
			<?php
				}
			?>
			</div>
		</div>
	</div>
	
</form>