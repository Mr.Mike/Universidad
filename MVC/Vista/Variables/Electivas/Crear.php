<ol class="breadcrumb">
	<li class="breadcrumb-item">
	  <a>Crear Electiva</a>
	</li>
</ol>
<form method="post" action="<?php echo getUrl("Variables","Electivas","postCrear");?>">
	<div class="form-group">
		<div class="form-row">
			<div class="col-md-12">
			<label for="exampleInputName">Nombre de la Electiva</label>
			<input name="Nombre" class="form-control" id="exampleInputName" type="text" placeholder="ingresar nombre" required />
			
			<label for="selectTeacher">Instructor de la Electiva</label>
				<select name="Profesor" id="selectTeacher" class="form-control" required>
					<option>Seleccione</option>
				<?php
					for($i=0; $i<count($profes); $i++){
						echo "<option value=".$profes[$i][0].">".$profes[$i][3]." ".$profes[$i][4]."</option>";
					}
				?>
			</select>
			
			<label for="exampleInputStudens">Cupos de la Electiva</label>
			<input name="Cupos" class="form-control" id="exampleInputStudens" type="number" placeholder="ingresar numero de cupos" required />
		  
			<label for="exampleInputDescription">Descripcion de la Electiva</label>
			<textarea id="exampleInputDescription" rows="15" cols="100" name="Descripcion" class="form-control" minlength="20" placeholder="ingresar descripcion" required ></textarea>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<input class="btn btn-primary btn-block" type="submit" value="Crear" />
	</div>
</form>