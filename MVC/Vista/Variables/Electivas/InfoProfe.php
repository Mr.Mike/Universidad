<ol class="breadcrumb">
	<li class="breadcrumb-item">
	  <a>Informaci&oacute;n Profesor</a>
	</li>
</ol>
<form>
	<div class="form-group">
		<div class="form-row">
			<div class="col-md-12">
			<?php
				for($i=0; $i<count($profe); $i++){
			?>
				<input name="Id" type="hidden" value="<?php echo $electivas[$i][0]?>"/>
				
				<label for="exampleInputName">Nombre del Profesor</label>
				<input name="Nombre" class="form-control" id="exampleInputName" type="text" value="<?php echo $profe[$i][3]." ".$profe[$i][4] ?>" readonly />
				
				<label>Electivas del Profesor</label>
				<ol>
					<?php
						for($i=0; $i<count($electivas); $i++){
					?>
						<a href="<?php echo getUrl('Variables','Electivas','getInfo',array("id"=>$electivas[$i][8]));?>"><li><?php echo $electivas[$i][10];?></li></a>
					<?php
						}
					?>
				</ol>
			<?php
				}
			?>
			</div>
		</div>
	</div>
	
</form>