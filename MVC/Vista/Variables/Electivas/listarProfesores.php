 <ol class="breadcrumb">
	<li class="breadcrumb-item">
	  <a>Profesores</a>
	</li>
</ol>
 <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Listado</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Consultar Electivas</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
				  <th>Nombre</th>
                  <th>Consultar Electivas</th>
                </tr>
              </tfoot>
              <tbody>
				<?php
					for($i=0; $i<count($profes); $i++){
				?>
						<tr>
						  <td><?php echo $profes[$i][3]." ".$profes[$i][4];?></td>
						  
						  <td><a href="<?php echo getUrl('Variables','Electivas','getInfoProfe',array("id"=>$profes[$i][0]));?>" > <button class='btn btn-primary btn-block' >Consultar</button></a></td>
						</tr>
				<?php	
					}
				?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted"></div>
    </div>
    
    