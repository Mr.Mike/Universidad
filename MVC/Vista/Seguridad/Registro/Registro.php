<?php
	include_once '../../../Libreria/helpers.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="../../../Web/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../../Web/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../../Web/css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Registro</div>
      <div class="card-body">
        <form method="post" action="<?php getUrl("Seguridad","Login","");?>">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputName">Nombre(s)</label>
                <input class="form-control" id="exampleInputName" type="text" aria-describedby="nameHelp" placeholder="ingresar nombre(s)" required />
              </div>
              <div class="col-md-6">
                <label for="exampleInputLastName">Apellidos</label>
                <input class="form-control" id="exampleInputLastName" type="text" aria-describedby="nameHelp" placeholder="ingresar apellidos" required />
              </div>
            </div>
          </div>
          <div class="form-group">
            
          </div>
          <div class="form-group">
            <div class="form-row">
			  <div class="col-md-6">
				<label for="exampleInputEmail1">C&oacute;digo Universitario</label>
				<input class="form-control" id="exampleInputEmail1" type="number" min="1000000" aria-describedby="emailHelp" placeholder="ingresar codigo" required />
			  </div>
              <div class="col-md-6">
                <label for="exampleInputPassword1">Contraseña</label>
                <input class="form-control" id="exampleInputPassword1" type="password" placeholder="contraseña" required />
              </div>
            </div>
          </div>
          <button class="btn btn-primary btn-block" href="../../../Web/Login.php" type="submit">Registrar</button>
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="../../../Web/login.php">Volver Al Login</a>
        </div>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="../../../Web/vendor/jquery/jquery.min.js"></script>
  <script src="../../../Web/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="../../../Web/vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
