<?php

function redirect($url){

    echo "<script type='text/javascript'>"
            . "window.location.href='$url';"
      . "</script>";
}

function getUrl($modulo,$controlador,$funcion,$parametros=false,$pagina=false){
    if($pagina==false){
        $pagina="index";
    }

    $url="$pagina.php?modulo=$modulo&controlador=$controlador&funcion=$funcion";
    if($parametros!=false){
        foreach($parametros as $key=>$valor){
            $url.="&$key=$valor";
        }
    }
    return $url;
}

function resolve(){
    $modulo= ucwords($_GET['modulo']);
    $controlador=ucwords($_GET['controlador']);
    $funcion=$_GET['funcion'];

    if(is_dir("../Controlador/$modulo")){
        if(file_exists("../Controlador/$modulo/" . $controlador . "Controlador.php")){
            include_once "../Controlador/$modulo/" . $controlador . "Controlador.php";
            $nombreClase=$controlador."Controlador";
            $objClase=new $nombreClase();
            if(method_exists($objClase, $funcion)){
                $objClase->$funcion();
            }
            else{
                echo "la funci&oacute;n especificado no existe";
            }
        }
        else{
            echo "El controlador especificado no existe";
        }
    }
    else{
        echo "El m&oacute;dulo especificado no existe";
    }
}

function alert($mensaje){

    echo "<script type='text/javascript'>"
    . "alert('$mensaje')"
            . "</script>";

}

//Función para escapar caracteres de una consulta
function escapar($link,$string){
    //alert("Hola");
    return pg_escape_string($link, $string);
}


function confirmar_consulta($resultados){
    if (!$resultados) {
        return false;
    }else{
        return true;
    }
}
?>
