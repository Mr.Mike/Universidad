<?php

class Connection
{
	private $host;
	private $username;
	private $password;
	private $database;
	private $port;
	private $link;

	function __construct(){
		$this->setConnect();
		$this->connect();
	}

	private function setConnect(){
		require("Conf.php");
		$this->host=$host;
		$this->username=$username;
		$this->password=$password;
		$this->database=$database;
		$this->port=$port;

	}

	private function connect(){
		$this->link=pg_connect("host='$this->host' port='$this->port' dbname='$this->database' user='$this->username' password='$this->password' ");
		if(!$this->link){
    	die(pg_errormessage($this->link));
		}
	}

	public function getConnect(){
		return $this->link;
	}

	public function close(){
		pg_close($this->link);
	}
        //Función que ejecutará la consulta OJO! Retorna una matriz con índices numéricos
	public function execute($statement){
		$ejecucion=pg_query($this->link,$statement);
		$result= array();
		if(!pg_errormessage($this->link)){
      while($row=pg_fetch_row($ejecucion)){
        array_push($result,$row);
      }

		}
		else{
      echo pg_errormessage($this->link);
		}
		return $result;
	}

  //Funcion que genera el incremento de la tabla que se especifíque
	public function getIncrement($table){
		$sql="SELECT COUNT(*) FROM $table";
		$resultado=$this->execute($sql);
		return $resultado[0][0]+1;
	}

	//Función para obtener el último id ingresado
	public function getLastId($table,$id){
		$sql="SELECT MAX($id) FROM $table";
		$resultado=$this->execute($sql);
		return $resultado[0][0];
	}

	//Función para obtener el número de columnas presentes en una tabla
	public function getColumnNumber($sql){
		$resultado=$this->execute($sql);
		return $resultado.length;
	}
}

?>
