<?php

include_once '../Modelo/Seguridad/LoginModel.php';

class LoginControlador{

    public function registrar(){
		$ConexionLogin= new LoginModel();
		
		$nombre = $_POST['Nombre'];
		$apellido = $_POST['Apellido'];
		$codigo = $_POST['Codigo'];
		$contraseña = sha1(md5($_POST['Contraseña']));
		$tipo = $_POST['Tipo'];
		$doc = $_POST['Doc'];
		
		$confir = $this -> confirmarCodigo($codigo);
		
		if($confir == true){
			$sql = "insert into usuario values($doc,3,$tipo,'$nombre','$apellido',$codigo,$doc,'$contraseña')";
			$resultado = $ConexionLogin->consultar($sql);
		}else{
			echo"<script>alert('El código del usuario ya existe. Por favor intente otro código universitario')</script>";
		}
		
		redirect("../Web/Index.php");
	}
	
	public function confirmarCodigo($codigo){
		$ConexionLogin= new LoginModel();
		
		$cod="";
		$retorno=false;
		$sql = "select*from usuario where usu_codigo_uni=$codigo";
		
		$resultado = $ConexionLogin->consultar($sql);
		
		while($res= pg_fetch_array($resultado)){
			$cod = $res['usu_id'];
		}
		
		if($cod == null){
			$retorno=true;
		}
		
		return $retorno;
	}
	
	public function inicio(){
		$ConexionLogin= new LoginModel();
		
		
		$codigo=$_POST['Codigo'];
		$contraseña=sha1(md5($_POST['Contraseña']));
		
		$confirmarCuenta = $this -> confirmarCuenta($codigo,$contraseña);
		
		if($confirmarCuenta == true){
			$datos = $this -> obtenerDatos($codigo,$contraseña);
			
			for($i=0; $i<count($datos); $i++){
				$_SESSION['codigo']=$datos[$i][0];
				$_SESSION['usuario']=$datos[$i][3];
				$_SESSION['tipo']=$datos[$i][1];
			}
			
			$_SESSION['iniciado']=true;
		}else{
			echo"<script>alert('Usuario y/o Contraseña incorrecta')</script>";
		}
		
		redirect("../Web/Index.php");
	}
	
	public function confirmarCuenta($codigo,$contraseña){
		$ConexionLogin= new LoginModel();
		
		$cod="";
		$retorno=true;
		$sql="select*from usuario where usu_contrasena='$contraseña' and usu_codigo_uni=$codigo";
		
		$resultado = $ConexionLogin->consultar($sql);
		
		while($res= pg_fetch_array($resultado)){
			$cod = $res['usu_id'];
		}
		
		if($cod == null){
			$retorno=false;
		}
		
		return $retorno;
		
	}
	
	public function obtenerDatos($codigo,$contraseña){
		$ConexionLogin= new LoginModel();
		
		$sql="select*from usuario where usu_contrasena='$contraseña' and usu_codigo_uni=$codigo";
		
		$resultado = $ConexionLogin->consultarArray($sql);
		
		return $resultado;
	}
	
	public function cerrar(){
        $_SESSION['iniciado']=false;
        redirect("../Web/Index.php");
    }
}
