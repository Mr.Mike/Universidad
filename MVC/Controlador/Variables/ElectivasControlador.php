<?php
include_once '../Modelo/Variables/ElectivasModel.php';

class ElectivasControlador extends ElectivasModel{

    public function getCrear(){
        
        $ConexionElectivas = new ElectivasModel();
        $sql = "select*from usuario where tu_id=2";
        $profes = $ConexionElectivas->consultarArray($sql);
		
        include_once '../Vista/Variables/Electivas/Crear.php';
    }

    public function postCrear(){
		
		$ConexionElectivas = new ElectivasModel();
		$id = $ConexionElectivas->autoIncrement("electivas","ele_id");
        $nombre = $_POST['Nombre'];
        $profe = $_POST['Profesor'];
        $desc = $_POST['Descripcion'];
        $cupos = $_POST['Cupos'];
        
        $sql = "insert into electivas values($id,$profe,'$nombre','$desc',$cupos,0)";
        
        
        $ConexionElectivas->insertar($sql);
        redirect(getUrl("Variables","Electivas","Listar"));
    }


    public function Listar(){
		
		$ConexionElectivas = new ElectivasModel();
        $sql = "select*from electivas ORDER BY ele_id";
        $electivas = $ConexionElectivas->consultarArray($sql);
		
        include_once '../Vista/Variables/Electivas/Listar.php';
    }
	
	public function listarParaUsu(){
		
		$ConexionElectivas = new ElectivasModel();
        $sql = "select*from electivas ORDER BY ele_id";
        $electivas = $ConexionElectivas->consultarArray($sql);
		
        include_once '../Vista/Variables/Electivas/ListarPUsu.php';
    }
	
	public function postInscripcion(){
		
		$ConexionElectivas = new ElectivasModel();
		$id = $ConexionElectivas->autoIncrement("usuario_electivas","ue_id");
		$alum = $ConexionElectivas->autoIncrement("electivas","ele_alumnos");
		$cupos = $ConexionElectivas->autoDecrement("electivas","ele_cupos");
        $idUsu = $_POST['idUsu'];
        $idEle = $_POST['idEle'];
		$mensaje="";
		$confirmarInscripcion = $this -> confirmarInscripcion($idUsu,$idEle);
		
		if($confirmarInscripcion==true){
			$sql = "INSERT INTO usuario_electivas VALUES ($id,$idUsu,$idEle)";
			$ConexionElectivas->consultar($sql);
			$sql = "UPDATE electivas set ele_alumnos=$alum,ele_cupos=$cupos where ele_id=$idEle";
			$ConexionElectivas->consultar($sql);
		}
	}
	
	public function confirmarInscripcion($idUsu,$idEle){
		$ConexionElectivas = new ElectivasModel();
		
		$cod="";
		$retorno=false;
		$sql="select*from usuario_electivas where usu_id=$idUsu and ele_id=$idEle";
		
		$resultado = $ConexionElectivas->consultar($sql);
		
		while($res= pg_fetch_array($resultado)){
			$cod = $res['eu_id'];
		}
		
		if($cod == null){
			$retorno=true;
		}
		
		return $retorno;
		
	}
	
	public function getInfo(){
		
		$ConexionElectivas = new ElectivasModel();
        $id = $_GET['id'];
		$sql = "select * from electivas,usuario where ele_id=$id and electivas.usu_id=usuario.usu_id";
        $electivas = $ConexionElectivas->consultarArray($sql);
		$sql = "select*from usuario,electivas,usuario_electivas where electivas.ele_id=$id and usuario.usu_id=usuario_electivas.usu_id and electivas.ele_id=usuario_electivas.ele_id";
		$alumnos = $ConexionElectivas->consultarArray($sql);
		
        include_once '../Vista/Variables/Electivas/Info.php';
    }
	
	public function getProfesores(){
		
		$ConexionElectivas = new ElectivasModel();
		$sql = "select*from usuario where tu_id=2";
		$profes = $ConexionElectivas->consultarArray($sql);
		
        include_once '../Vista/Variables/Electivas/listarProfesores.php';
    }
	
	public function getInfoProfe(){
		
		$ConexionElectivas = new ElectivasModel();
        $id = $_GET['id'];
		$sql = "select*from usuario where usuario.usu_id=$id";
		$profe = $ConexionElectivas->consultarArray($sql);
		
		$sql = "select*from usuario,electivas where usuario.usu_id=$id and usuario.usu_id=electivas.usu_id  ORDER BY ele_id";
		$electivas = $ConexionElectivas->consultarArray($sql);
		
        include_once '../Vista/Variables/Electivas/InfoProfe.php';
    }

    public function getEditar(){
		
		$ConexionElectivas = new ElectivasModel();
        $id = $_GET['id'];
		$sql = "select * from electivas,usuario where ele_id=1 and electivas.usu_id=usuario.usu_id";
        $electivas = $ConexionElectivas->consultarArray($sql);
		$sql = "select*from usuario where tu_id=2";
		$profes = $ConexionElectivas->consultarArray($sql);
		
        include_once '../Vista/Variables/Electivas/Actualizar.php';
    }

    public function postEditar(){
		
		$ConexionElectivas = new ElectivasModel();
		$id = $_POST['Id'];
        $nombre = $_POST['Nombre'];
        $profe = $_POST['Profesor'];
        $desc = $_POST['Descripcion'];
        $cupos = $_POST['Cupos'];
		
        $sql = "update electivas set ele_nombre='$nombre',usu_id=$profe,ele_descripcion='$desc',ele_cupos=$cupos where ele_id=$id";
        $ConexionElectivas->editar($sql);
		
		redirect(getUrl("Variables","Electivas","Listar"));
    }


    public function postEliminar(){
		
        $ConexionElectivas = new ElectivasModel();
        $id = $_POST['id'];
        $sql = "DELETE FROM electivas WHERE ele_id = $id";
        $ConexionElectivas->eliminar($sql);
    }

}

?>
