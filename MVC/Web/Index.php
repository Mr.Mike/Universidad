<?php
session_start();

if(($_SESSION['iniciado'])==true){
	if(($_SESSION['tipo'])==1){
		include_once '../Libreria/helpers.php';
		include_once '../Vista/Partes/Head.php';	
?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="Index.php">Universidad Salsipuedes</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Inicio">
          <a class="nav-link" href="Index.php">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Inicio</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Usuarios">
          <a class="nav-link" href="<?php echo getUrl("Variables","Usuarios","listar");?>">
            <i class="fa fa-fw fa-users"></i>
            <span class="nav-link-text">Usuarios</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Electivas">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-file"></i>
            <span class="nav-link-text">Electivas</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseExamplePages">
            <li>
              <a href="<?php echo getUrl("Variables","Electivas","getCrear");?>">Crear</a>
            </li>
            <li>
              <a href="<?php echo getUrl("Variables","Electivas","Listar");?>">Listar</a>
            </li>
          </ul>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModalSalir">
            <i class="fa fa-fw fa-sign-out"></i>Cerrar Sesi&oacute;n</a>
        </li>
      </ul>
    </div>
  </nav>
    <div class="content-wrapper">
		<div class="container-fluid">
			<?php
				if(isset($_GET['modulo'])){
					resolve ();
				}else{
			?>
			<ol class="breadcrumb">
				<li class="breadcrumb-item">
				  <a>Inicio</a>
				</li>
			</ol>
			<div class="row">
				<div class="col-xl-6 col-sm-6 mb-3">
				  <div class="card text-white bg-primary o-hidden h-100">
					<div class="card-body">
					  <div class="card-body-icon">
						<i class="fa fa-fw fa-list"></i>
					  </div>
					  <div class="mr-5">Crear Electiva</div>
					</div>
					<a class="card-footer text-white clearfix small z-1" href="<?php echo getUrl("Variables","Electivas","getCrear");?>">
					  <span class="float-left">Ir</span>
					  <span class="float-right">
						<i class="fa fa-angle-right"></i>
					  </span>
					</a>
				  </div>
				</div>
				<div class="col-xl-6 col-sm-6 mb-3">
				  <div class="card text-white bg-warning o-hidden h-100">
					<div class="card-body">
					  <div class="card-body-icon">
						<i class="fa fa-fw fa-users"></i>
					  </div>
					  <div class="mr-5">Usuarios</div>
					</div>
					<a class="card-footer text-white clearfix small z-1" href="<?php echo getUrl("Variables","Usuarios","listar");?>">
					  <span class="float-left">Ir</span>
					  <span class="float-right">
						<i class="fa fa-angle-right"></i>
					  </span>
					</a>
				  </div>
				</div>
			<?php
				}
			?>
			</div>
		</div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Universidad Salsipuedes 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModalSalir" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Listo Para Salir?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Presiona "Salir" para cerrar sesi&oacute;n.</div>
			  <div class="modal-footer">
				<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
				<a class="btn btn-primary" href="<?php echo getUrl("Seguridad","Login","cerrar");?>">Salir</a>
			  </div>
        </div>
      </div>
    </div>
  </div>
</body>
<?php
		include_once '../Vista/Partes/footer.php';
	}
	if(($_SESSION['tipo'])==2 || ($_SESSION['tipo'])==3){
		include_once '../Libreria/helpers.php';
		include_once '../Vista/Partes/Head.php';	
?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="Index.php">Universidad Salsipuedes</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="Index.php">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Inicio</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Electivas">
          <a class="nav-link" href="<?php echo getUrl("Variables","Electivas","listarParaUsu");?>" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-file"></i>
            <span class="nav-link-text">Electivas</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModalSalir">
            <i class="fa fa-fw fa-sign-out"></i>Cerrar Sesi&oacute;n</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="content-wrapper">
    <div class="container-fluid">
        <?php
		 if(isset($_GET['modulo'])){
			resolve ();
		 }else{
			 ?>
			<ol class="breadcrumb">
				<li class="breadcrumb-item">
				  <a>Inicio</a>
				</li>
			</ol>
			<div class="row">
				<div class="col-xl-6 col-sm-6 mb-3">
				  <div class="card text-white bg-primary o-hidden h-100">
					<div class="card-body">
					  <div class="card-body-icon">
						<i class="fa fa-fw fa-list"></i>
					  </div>
					  <div class="mr-5">Electivas</div>
					</div>
					<a class="card-footer text-white clearfix small z-1" href="<?php echo getUrl("Variables","Electivas","listarParaUsu");?>">
					  <span class="float-left">Ir</span>
					  <span class="float-right">
						<i class="fa fa-angle-right"></i>
					  </span>
					</a>
				  </div>
				</div>
				<div class="col-xl-6 col-sm-6 mb-3">
				  <div class="card text-white bg-warning o-hidden h-100">
					<div class="card-body">
					  <div class="card-body-icon">
						<i class="fa fa-fw fa-users"></i>
					  </div>
					  <div class="mr-5">Profesores</div>
					</div>
					<a class="card-footer text-white clearfix small z-1" href="<?php echo getUrl("Variables","Electivas","getProfesores");?>">
					  <span class="float-left">Ir</span>
					  <span class="float-right">
						<i class="fa fa-angle-right"></i>
					  </span>
					</a>
				  </div>
				</div>
			<?php
			 }
			?>
			</div>
    </div>
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Universidad Salsipuedes 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModalSalir" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Listo Para Salir?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Presiona "Salir" para cerrar sesi&oacute;n.</div>
			  <div class="modal-footer">
				<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
				<a class="btn btn-primary" href="<?php echo getUrl("Seguridad","Login","cerrar");?>">Salir</a>
			  </div>
        </div>
      </div>
    </div>
  </div>
</body>
<?php
		include_once '../Vista/Partes/footer.php';
	}
}else{	
		include_once '../Modelo/Seguridad/LoginModel.php';

		$Conexion = new LoginModel();

		$sql = "SELECT*FROM Tipo_Documento";

		$Tipo_D = $Conexion->consultarArray($sql);
	
		include_once '../Libreria/helpers.php';
		include_once '../Vista/Partes/Head.php';	
?>
		<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="Index.php">Universidad Salsipuedes</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Inicio">
          <a class="nav-link" href="Index.php">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Inicio</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
		<li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#ModalIngreso">
            <i class="fa fa-fw fa-sign-in"></i>Ingresar</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#ModalRegistro">
            <i class="fa fa-fw fa-user-plus"></i>Registrarse</a>
        </li>	
      </ul>
    </div>
  </nav>
	<div class="content-wrapper">
		<div class="content-fluid">
			<?php
			 if(isset($_GET['modulo'])){
				resolve ();
			 }else{
				 ?>
				 
			<ol class="breadcrumb">
				<li class="breadcrumb-item">
				  <a>Inicio</a>
				</li>
			</ol>
			<div class="row">
				<div class="card mb-3 text-white bg-primary o-hidden ">
					<div class="card-header">
					   Unete Ahora
					</div>
					<div class="card-body">
					  <div class="table-responsive">
						<a href="#">
							<img class="card-img-top" height="500px" src="img/Universidad.png" alt="Universidad">
						</a>
					  </div>
					</div>
					<a class="card-footer text-white clearfix small z-1" data-toggle="modal" data-target="#ModalRegistro">
					  <span class="float-left">Unirse</span>
					  <span class="float-right">
						<i class="fa fa-angle-right"></i>
					  </span>
					</a>
				</div>
		  <?php
			 }
			?>
		  </div>
		</div>
	
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Universidad Salsipuedes 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->

    <div class="modal fade" id="ModalRegistro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">REGISTRO</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">×</span>
				</button>
			  </div>
			  <div class="modal-body">
				
				<form method="post" action="<?php echo getUrl("Seguridad","Login","registrar");?>">
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-12">
							<label for="exampleInputName">Nombre(s)</label>
							<input name="Nombre" class="form-control" id="exampleInputName" type="text" placeholder="ingresar nombre(s)" required />
						  
							<label for="exampleInputLastName">Apellidos</label>
							<input name="Apellido" class="form-control" id="exampleInputLastName" type="text"  placeholder="ingresar apellidos" required />
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-12">
								<label for="exampleInputIdType">Tipo de Documento</label>
								<select id="exampleInputIdType" name="Tipo" class="form-control" required>
									<option value="" >Seleccione...</option> 
									<?php  
										for($i=0; $i<count($Tipo_D); $i++){
											echo "<option value=".$Tipo_D[$i][0].">".$Tipo_D[$i][1]."</option>";
										}
									?>
								</select>
								<label for="exampleInputDoc">Documento de Identidad</label>
								<input name="Doc" class="form-control" id="exampleInputDoc" type="number" placeholder="documento" required />
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-row">
							<div class="col-md-12">
							<label for="exampleInputCodigo">C&oacute;digo Universitario</label>
							<input name="Codigo" class="form-control" id="exampleInputCodigo" type="number" min="1000000" placeholder="ingresar codigo" required />
						  
							<label for="exampleInputPassword">Contraseña</label>
							<input name="Contraseña" class="form-control" id="exampleInputPassword" type="password" placeholder="contraseña" required />
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<input class="btn btn-primary btn-block" type="submit" value="Registrar" />
					</div>
				</form>
			  </div>
		  </div>
		</div>
	  </div>
	  <div class="modal fade" id="ModalIngreso" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">INGRESO</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">×</span>
				</button>
			  </div>
			  <div class="modal-body">
				<form method="post" action="<?php echo getUrl("Seguridad","Login","inicio");?>">
					<div class="form-group">
						<div class="form-row">
						<div class="col-md-12">
							<label for="exampleInputCodigo1">C&oacute;digo Universitario</label>
							<input name="Codigo" class="form-control" id="exampleInputCodigo1" type="number" min="1000000" placeholder="codigo universitario" required />
					  
							<label for="exampleInputPassword1">Contraseña</label>
							<input name="Contraseña" class="form-control" id="exampleInputPassword1" type="Password" placeholder="contraseña" required />
						</div>
						</div>
					</div>
					<div class="modal-footer">
						<input class="btn btn-primary btn-block" type="submit" value="Ingresar" />
					</div>
				</form>
			  </div>
		  </div>
		</div>
	  </div>
  </div>
 </body>
<?php
		include_once '../Vista/Partes/footer.php';	
	}
?>
</html>
